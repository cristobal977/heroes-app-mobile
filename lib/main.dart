import 'package:flutter/material.dart';
import 'package:super_heroes/view/home_screen.dart';

void main() {
  runApp(HeroeApp());
}

class HeroeApp extends StatelessWidget {
  const HeroeApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Heroes App",
      theme: ThemeData(
        primaryColor: Colors.deepPurple[800],
      ),
      home: HomeScreen(),
    );
  }
}
