import 'package:flutter/material.dart';
import 'package:super_heroes/models/heroe.dart';
import 'package:super_heroes/util/fontStyle.dart';

// ignore: must_be_immutable
class HeroeScreen extends StatelessWidget {
  Heroe heroe;

  HeroeScreen(this.heroe);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(heroe.name),
      ),
      body: ListView(
        children: [
          Image.network(heroe.imageHeroe.url),
          buildPowerstats(),
          buildBiography(),
          buildAppearance(),
          buildWork(),
          buildConnections(),
        ],
      ),
    );
  }

  Padding buildPowerstats() {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Powerstats", style: textCategoria()),
          Text("Intelligence: " + heroe.powerStats.intelligence),
          Text("Strength: " + heroe.powerStats.strength),
          Text("Speed: " + heroe.powerStats.speed),
          Text("Durability: " + heroe.powerStats.durability),
          Text("Power: " + heroe.powerStats.power),
          Text("Combat: " + heroe.powerStats.combat),
        ],
      ),
    );
  }

  Padding buildBiography() {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Biography", style: textCategoria()),
          Text("Full name: " + heroe.biography.fullName),
          Text("Alter egos: " + heroe.biography.alterEgos),
          Text("Aliases: " + heroe.biography.aliases.toString()),
          Text("Place-of-birth: " + heroe.biography.placeOfBirth),
          Text("First-appearance: " + heroe.biography.firstApperance),
          Text("Publisher: " + heroe.biography.publisher),
          Text("Alignment: " + heroe.biography.alignment),
        ],
      ),
    );
  }

  Padding buildAppearance() {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Appearance", style: textCategoria()),
          Text("Gender: " + heroe.appearance.gender),
          Text("Race: " + heroe.appearance.race),
          Text("Height: " + heroe.appearance.height.toString()),
          Text("Weight: " + heroe.appearance.weight.toString()),
          Text("Eye color: " + heroe.appearance.eyeColor),
          Text("Hair color: " + heroe.appearance.hairColor),
        ],
      ),
    );
  }

  Padding buildWork() {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Work", style: textCategoria()),
          Text("Occupation: " + heroe.work.occupation),
          Text("Base: " + heroe.work.base),
        ],
      ),
    );
  }

  Padding buildConnections() {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Connections", style: textCategoria()),
          Text("Group affiliation: " + heroe.connections.groupAffiliation),
          Text("Relatives: " + heroe.connections.relatives),
        ],
      ),
    );
  }
}
