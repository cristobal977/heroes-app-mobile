import 'package:flutter/material.dart';
import 'package:super_heroes/models/heroe.dart';
import 'package:super_heroes/service/heroe_api.dart';
import 'package:super_heroes/util/fontStyle.dart';
import 'package:super_heroes/view/heroe_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

// https://superheroapi.com/api/10224074261873083/70

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HeroesApp"),
        elevation: 0,
      ),
      body: ListView(
        children:
            List.generate(20, (index) => buildHeroe((index + 1).toString())),
      ),
    );
  }

  FutureBuilder<Heroe> buildHeroe(String id) {
    return FutureBuilder<Heroe>(
      future: getHeroe(id),
      builder: (BuildContext context, AsyncSnapshot<Heroe> snapshot) {
        if (!snapshot.hasData) {
          return Text("");
        }
        final Heroe heroe = snapshot.data;
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HeroeScreen(heroe),
              ),
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(15),
                child: Text(
                  heroe.name,
                  style: textCategoria(),
                ),
              ),
              Image(image: NetworkImage(heroe.imageHeroe.url)),
              //SizedBox(height: 15)
            ],
          ),
        );
      },
    );
  }
}
