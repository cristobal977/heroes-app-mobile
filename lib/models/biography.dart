class Biography {
  final String fullName;
  final String alterEgos;
  final List<dynamic> aliases;
  final String placeOfBirth;
  final String firstApperance;
  final String publisher;
  final String alignment;

  Biography({
    this.fullName,
    this.alterEgos,
    this.aliases,
    this.placeOfBirth,
    this.firstApperance,
    this.publisher,
    this.alignment,
  });

  factory Biography.fromJson(Map<String, dynamic> json) {
    return Biography(
      fullName: json["full-name"],
      alterEgos: json["alter-egos"],
      aliases: json["aliases"],
      placeOfBirth: json["place-of-birth"],
      firstApperance: json["first-appearance"],
      publisher: json["publisher"],
      alignment: json["alignment"],
    );
  }
}
