import 'package:super_heroes/models/appearance.dart';
import 'package:super_heroes/models/biography.dart';
import 'package:super_heroes/models/connections.dart';
import 'package:super_heroes/models/image_heroe.dart';
import 'package:super_heroes/models/power_stats.dart';
import 'package:super_heroes/models/work.dart';

class Heroe {
  final String response;
  final String id;
  final String name;
  final PowerStats powerStats;
  final Biography biography;
  final Appearance appearance;
  final Work work;
  final Connections connections;
  final ImageHeroe imageHeroe;

  Heroe({
    this.response,
    this.id,
    this.name,
    this.powerStats,
    this.biography,
    this.appearance,
    this.work,
    this.connections,
    this.imageHeroe,
  });

  factory Heroe.fromJson(Map<String, dynamic> json) {
    return Heroe(
      response: json["id"],
      id: json["id"],
      name: json["name"],
      powerStats: PowerStats.fromJson(json["powerstats"]),
      biography: Biography.fromJson(json["biography"]),
      appearance: Appearance.fromJson(json["appearance"]),
      work: Work.fromJson(json["work"]),
      connections: Connections.fromJson(json["connections"]),
      imageHeroe: ImageHeroe.fromJson(json["image"]),
    );
  }
}
