class Work {
  final String occupation;
  final String base;

  Work({
    this.occupation,
    this.base,
  });

  factory Work.fromJson(Map<String, dynamic> json) {
    return Work(
      occupation: json["occupation"],
      base: json["base"],
    );
  }
}
