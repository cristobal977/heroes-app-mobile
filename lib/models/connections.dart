class Connections {
  final String groupAffiliation;
  final String relatives;

  Connections({
    this.groupAffiliation,
    this.relatives,
  });

  factory Connections.fromJson(Map<String, dynamic> json) {
    return Connections(
      groupAffiliation: json["group-affiliation"],
      relatives: json["relatives"],
    );
  }
}
