class Appearance {
  final String gender;
  final String race;
  final List<dynamic> height;
  final List<dynamic> weight;
  final String eyeColor;
  final String hairColor;

  Appearance({
    this.gender,
    this.race,
    this.height,
    this.weight,
    this.eyeColor,
    this.hairColor,
  });

  factory Appearance.fromJson(Map<String, dynamic> json) {
    return Appearance(
      gender: json["gender"],
      race: json["race"],
      height: json["height"],
      weight: json["weight"],
      eyeColor: json["eye-color"],
      hairColor: json["hair-color"],
    );
  }
}
