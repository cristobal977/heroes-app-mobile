class ImageHeroe {
  final String url;

  ImageHeroe({
    this.url,
  });

  factory ImageHeroe.fromJson(Map<String, dynamic> json) {
    return ImageHeroe(
      url: json["url"],
    );
  }
}
