import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:super_heroes/models/heroe.dart';

String apiUrl = "https://www.superheroapi.com/api.php/10224074261873083/";

Future<Heroe> getHeroe(String id) async {
  final response = await http.get(apiUrl + id);
  if (response.statusCode == 200) {
    return Heroe.fromJson(json.decode(response.body));
  } else {
    throw Exception("Fallo Heroe");
  }
}
